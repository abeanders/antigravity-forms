<?php

/**
 * Plugin Name: Antigravity Forms
 * Description: Hides a Gravity Form widget, to then be shown in a Bootstrap modal window. Use the shortcode '[antigravity]...[/antigravity]' to hide the gravity form that would have shown on the current page, and create a button to show the form in a modal window.
 * Version: 1.0
 * Author: Abe Millett
 * License: GPL3
 */

/**
 * 1) Registers a shortcode that outputs the passed content wrapped in a button tag
 * 2) Instances of a Gravity Form widget on front-end pages are wrapped in markup for bootstrap modal window, given the following conditions:
 *  a) The current page is a front-end page
 *  b) The shortcode is used anywhere on the current page or in a widget
 *  c) A Gravity Form widget is used on the current page
 */


$aam_ag_shortcode_on_page = false; // a flag used to detect a shortcode on the current page...


add_shortcode( 'antigravity', 'aam_ag_shortcode_handler' );

function aam_ag_shortcode_handler( $atts, $content ) {

	global $aam_ag_shortcode_on_page;

	extract( shortcode_atts( array(
		'color' => 'default'
	), $atts ) );

	$string = '<button class="btn';

	switch( $color ) {
		case 'default':
			break;
		case 'blue':
			$string .= ' btn-primary';
			break;
		case 'green':
			$string .= ' btn-success';
			break;
		case 'teal':
			$string .= ' btn-info';
			break;
		case 'orange':
			$string .= ' btn-warning';
			break;
		case 'red':
			$string .= ' btn-danger';
			break;
		case 'link':
			$string .= ' btn-link';
			break;
	}

	$string .= '" data-toggle="modal" data-target="#antigravity">' . $content . '</button>';
	$aam_ag_shortcode_on_page = true; // shortcode has been used, set flag to true...
	return $string;

}


if ( ! is_admin() )
	add_filter( 'dynamic_sidebar_params', 'aam_ag_modify_gform' );

function aam_ag_modify_gform( $params ) {

	global $aam_ag_shortcode_on_page;

	if ( $aam_ag_shortcode_on_page && substr( $params[0]['widget_id'], 0, 12 ) == 'gform_widget' ) { // shortcode has been used and gform widget is on the page...
		$params[0]['before_widget'] = '<div class="modal fade hide" id="antigravity"><div class="modal-dialog"><div class="modal-content">';
		$params[0]['before_title'] = '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title">';
		$params[0]['after_title'] = '</h4></div><div class="modal-body" style="max-height: 100%;">';
		$params[0]['after_widget'] = '</div></div></div></div>';
		return $params; // return modified $params...
	}

	return $params; // return un-modified $params...

}

// fixes modal behind backdrop issue in IE7...
// if ( is IE7 )
add_action( 'wp_footer', 'aam_ag_append_js', 100 );

function aam_ag_append_js() {

	echo( '<script type="text/javascript">jQuery(".modal").appendTo(jQuery("body"));</script>' . PHP_EOL );

}


?>