Antigravity Forms
=================

A WordPress plugin that puts the content of a Gravity Form widget in a Bootstrap modal window.

Use the shortcode '[antigravity]...[/antigravity]' in a widget or in the page content. This will hide a gravity form that would have shown on the current page, and creates a button that will show the form in a modal window. The content passed in the shortcode will become the content inside the button tag.

Include the attribute "color" in the shortcode to select from the default Bootstrap colors. The options are 'blue', 'green', 'teal', 'orange', 'red', or 'link' (bootstrap link style). The default value is 'default', resulting in the Bootstrap default grey.

Code Flow:
----------

1. Registers a shortcode that outputs the passed content wrapped in a button tag
2. Instances of a Gravity Form widget on front-end pages are wrapped in markup for bootstrap modal window, given the following conditions:
    + The current page is a front-end page
    + The shortcode is used anywhere on the current page or in a widget
    + A Gravity Form widget is used on the current page